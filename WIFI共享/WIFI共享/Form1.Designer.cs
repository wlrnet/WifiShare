﻿namespace WIFI共享
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.l2 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.Label();
            this.pass = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.start_or_close = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // l2
            // 
            this.l2.AllowDrop = true;
            this.l2.AutoSize = true;
            this.l2.BackColor = System.Drawing.Color.Silver;
            this.l2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.l2.Location = new System.Drawing.Point(236, 16);
            this.l2.MinimumSize = new System.Drawing.Size(25, 20);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(25, 20);
            this.l2.TabIndex = 2;
            this.l2.Text = "—";
            this.l2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l2.Click += new System.EventHandler(this.l2_Click);
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.BackColor = System.Drawing.Color.Silver;
            this.l1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.l1.Location = new System.Drawing.Point(272, 16);
            this.l1.MaximumSize = new System.Drawing.Size(50, 50);
            this.l1.MinimumSize = new System.Drawing.Size(25, 20);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(25, 20);
            this.l1.TabIndex = 3;
            this.l1.Text = "╳";
            this.l1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l1.Click += new System.EventHandler(this.l1_Click);
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.user.Location = new System.Drawing.Point(24, 69);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(58, 21);
            this.user.TabIndex = 4;
            this.user.Text = "用户名";
            // 
            // pass
            // 
            this.pass.AutoSize = true;
            this.pass.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pass.Location = new System.Drawing.Point(11, 117);
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(77, 21);
            this.pass.TabIndex = 5;
            this.pass.Text = "密码(8位)";
            // 
            // username
            // 
            this.username.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.username.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.username.Location = new System.Drawing.Point(91, 67);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(170, 29);
            this.username.TabIndex = 6;
            // 
            // password
            // 
            this.password.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.password.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.password.Location = new System.Drawing.Point(91, 114);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(168, 29);
            this.password.TabIndex = 7;
            this.password.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // start_or_close
            // 
            this.start_or_close.BackColor = System.Drawing.Color.Gainsboro;
            this.start_or_close.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.start_or_close.Location = new System.Drawing.Point(107, 166);
            this.start_or_close.Name = "start_or_close";
            this.start_or_close.Size = new System.Drawing.Size(101, 32);
            this.start_or_close.TabIndex = 8;
            this.start_or_close.Text = "开启WIFI";
            this.start_or_close.UseVisualStyleBackColor = false;
            this.start_or_close.Click += new System.EventHandler(this.start_or_close_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.退出ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 48);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(309, 230);
            this.Controls.Add(this.start_or_close);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.user);
            this.Controls.Add(this.l1);
            this.Controls.Add(this.l2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Label pass;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button start_or_close;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;

    }
}

